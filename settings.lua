data:extend({
	{
		type = "string-setting",
		name = "laserlines-default-color",
		order = "a",
		setting_type = "runtime-global",
		default_value = "red",
		allowed_values = { "red", "green", "blue", "yellow", "pink", "cyan", "white" },
	},
	{
		type = "int-setting",
		name = "laserlines-range",
		order = "b",
		setting_type = "runtime-global",
		default_value = 300,
		minimum_value = 1,
		maximum_value = 1000,
	},
	{
		type = "bool-setting",
		name = "laserlines-listen",
		order = "c",
		setting_type = "runtime-global",
		default_value = false,
	},
	{
		type = "bool-setting",
		name = "laserlines-obstructable",
		order = "d",
		setting_type = "runtime-global",
		default_value = true,
	},
})
