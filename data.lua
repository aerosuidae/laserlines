local cc = table.deepcopy(data.raw["constant-combinator"]["constant-combinator"])

data:extend({
	{
		type = "item",
		name = "laserlines-emitter",
		stack_size = 50,
		icon = "__laserlines__/emitter-icon.png",
		icon_size = 32,
		subgroup = "circuit-network",
		order = "z",
		place_result = "laserlines-emitter",
	},
	{
		type = "recipe",
		name = "laserlines-emitter",
		category = "crafting",
		subgroup = "circuit-network",
		enabled = false,
		icon = "__laserlines__/emitter-icon.png",
		icon_size = 32,
		ingredients = {
			{ type = "item", name = "iron-stick", amount = 1 },
			{ type = "item", name = "constant-combinator", amount = 1 },
		},
		results = {
			{ type = "item", name = "laserlines-emitter", amount = 1 },
		},
		hidden = false,
		energy_required = 1.0,
		order = "z",
	},
	{
		type = "technology",
		name = "laserlines",
		icon = "__laserlines__/tech.png",
		icon_size = 128,
		effects = {
			{ type = "unlock-recipe", recipe = "laserlines-emitter" },
		},
		prerequisites = {
			"circuit-network",
		},
		unit = {
			count = 100,
			ingredients = {
				{"automation-science-pack", 1},
				{"logistic-science-pack", 1},
			},
			time = 15
		},
		order = "a",
	},
	{
		type = "constant-combinator",
		name = "laserlines-emitter",
		flags = {
			"player-creation",
			"not-flammable",
		},
		selectable_in_game = true,
		minable = {
			result = "laserlines-emitter",
			mining_time = cc.minable.mining_time,
		},
		collision_mask = cc.collision_mask,
		collision_box = {{-0.4,-0.4},{0.4,0.4}},
		selection_box = {{-0.5,-0.5},{0.5,0.5}},
		icon = "__laserlines__/emitter-icon.png",
		icon_size = 32,
		tile_width = 1,
		tile_height = 1,
		item_slot_count = 1,
		sprites = {
			north = {
				filename = "__laserlines__/emitter-north-lr.png",
				width = 48,
				height = 48,
				priority = "high",
				hr_version = {
					filename = "__laserlines__/emitter-north-hr.png",
					width = 96,
					height = 96,
					priority = "high",
					scale = 0.5,
				}
			},
			east = {
				filename = "__laserlines__/emitter-east-lr.png",
				width = 48,
				height = 48,
				priority = "high",
				hr_version = {
					filename = "__laserlines__/emitter-east-hr.png",
					width = 96,
					height = 96,
					priority = "high",
					scale = 0.5,
				}
			},
			south = {
				filename = "__laserlines__/emitter-south-lr.png",
				width = 48,
				height = 48,
				priority = "high",
				hr_version = {
					filename = "__laserlines__/emitter-south-hr.png",
					width = 96,
					height = 96,
					priority = "high",
					scale = 0.5,
				}
			},
			west = {
				filename = "__laserlines__/emitter-west-lr.png",
				width = 48,
				height = 48,
				priority = "high",
				hr_version = {
					filename = "__laserlines__/emitter-west-hr.png",
					width = 96,
					height = 96,
					priority = "high",
					scale = 0.5,
				}
			},
		},
		activity_led_sprites = {
      east = {
        filename = "__laserlines__/nothing.png",
        frame_count = 1,
        height = 32,
        width = 32
      },
      north = {
        filename = "__laserlines__/nothing.png",
        frame_count = 1,
        height = 32,
        width = 32
      },
      south = {
        filename = "__laserlines__/nothing.png",
        frame_count = 1,
        height = 32,
        width = 32
      },
      west = {
        filename = "__laserlines__/nothing.png",
        frame_count = 1,
        height = 32,
        width = 32
      }
    },
		activity_led_light = { intensity = 0, size = 0 },
		activity_led_light_offsets = cc.activity_led_light_offsets,
		circuit_wire_connection_points = {
      {
        shadow = {
          green = { -0.45, -0.2 },
          red = { -0.45, -0.2 },
        },
        wire = {
          green = { -0.45, -0.2 },
          red = { -0.45, -0.2 },
        }
      },
      {
        shadow = {
          green = { 0.025, 0.2 },
          red = { 0.025, 0.2 },
        },
        wire = {
          green = { 0.025, 0.2 },
          red = { 0.025, 0.2 },
        }
      },
      {
        shadow = {
          green = { -0.45, -0.2 },
          red = { -0.45, -0.2 },
        },
        wire = {
          green = { -0.45, -0.2 },
          red = { -0.45, -0.2 },
        }
      },
      {
        shadow = {
          green = { 0.025, 0.2 },
          red = { 0.025, 0.2 },
        },
        wire = {
          green = { 0.025, 0.2 },
          red = { 0.025, 0.2 },
        }
      },
    },
		circuit_wire_max_distance = 9,
		corpse = "small-remnants",
	},
})

local function beam(name, tint)
	data:extend({
		{
			type = "beam",
			width = 0.5,
			name = "laserlines-beam-"..name,
			damage_interval = 1000,
			random_target_offset = false,
			flags = { "not-on-map" },
			body = {
				{
					animation_speed = 0.5,
					blend_mode = "additive",
					filename = "__laserlines__/hr-laser-body.png",
					flags = {"mipmap"},
					frame_count = 8,
					height = 12,
					line_length = 8,
					scale = 0.5,
					width = 64,
					tint = tint,
				}
			},
			head = {
				animation_speed = 0.5,
				blend_mode = "additive",
				filename = "__laserlines__/hr-laser-body.png",
				flags = {"mipmap"},
				frame_count = 8,
				height = 12,
				line_length = 8,
				scale = 0.5,
				width = 64,
				tint = tint,
			},
			tail = {
				animation_speed = 0.5,
				blend_mode = "additive",
				filename = "__laserlines__/hr-laser-body.png",
				flags = {"mipmap"},
				frame_count = 8,
				height = 12,
				line_length = 8,
				scale = 0.5,
				width = 64,
				tint = tint,
			},
		},
	})
end

beam("red", { r = 0.4, g = 0, b = 0, a = 1.0 })
beam("green", { r = 0, g = 0.3, b = 0, a = 1.0 })
beam("blue", { r = 0.1, g = 0.1, b = 1, a = 1.0 })
beam("yellow", { r = 0.3, g = 0.3, b = 0, a = 1.0 })
beam("pink", { r = 0.6, g = 0.1, b = 0.3, a = 1.0 })
beam("cyan", { r = 0.0, g = 0.3, b = 0.3, a = 1.0 })
beam("white", { r = 0.3, g = 0.3, b = 0.3, a = 1.0 })
