#include "colors.inc"
#include "textures.inc"
#include "shapes.inc"
#include "glass.inc"
#include "metals.inc"
#include "woods.inc"
#include "stones.inc"
#include "golds.inc"
#include "electric.inc"
#include "clock.inc"
#include "concrete.inc"
#include "camera.inc"
#include "lights.inc"

#declare F_Metallic  =
finish {
    ambient 0.25
    brilliance 4
    diffuse 0.5
    metallic
    specular 0.80
    roughness 1/80
    reflection 0
}

#declare T_WhiteGloss = texture { pigment { color White } finish { Metal } }
#declare T_BlackGloss = texture { pigment { color rgb <0.1,0.1,0.1> } }
#declare T_RedGloss = texture { pigment { color rgb <0.3,0.0,0.0> } }

object {
	union {
		cylinder {
			<0, 0, 0>, <0, 0.5, 0>, 0.9
			texture { T_BlackGloss }
		}
		object {
			difference {
				cylinder {
					<0, 0, 0>, <0, 0.5, 0>, 1.0
				}
				cylinder {
					<0, -0.1, 0>, <0, 0.7, 0>, 0.9
				}
			}
			texture { T_Copper_5E }
			//texture { T_RedGloss }
		}
		object {
			difference {
				box {
					<-0.975,0,-0.2>, <0.975,1.2,0.2>
					translate <0,0.5,0>
				}
				box {
					<-0.875,0,-0.3>, <0.875,1.3,0.3>
					translate <0,0.45,0>
				}
			}
			texture { T_Copper_5E }
			//texture { T_RedGloss }
		}
		cylinder {
			<0,-1,0>, <0,1,0>, 0.15
			texture { T_BlackGloss }
			rotate <0,0,90>
			translate <0,1.5,0>
		}
		sphere {
			<0,1.5,0>, 0.8
			texture { T_WhiteGloss }
		}
		object {
			difference {
				cylinder {
					<0, 0, 0>, <0, 0.9, 0>, 0.5
				}
				cylinder {
					<0, -0.1, 0>, <0, 1.1, 0>, 0.4
				}
			}
			texture { T_RedGloss }
			rotate <-90,0,0>
			translate <0,1.5,0>
		}
		cylinder {
			<0,0,0>, <0,0.1,0>, 0.4
			texture { Dark_Green_Glass }
			rotate <-90,0,0>
			translate <0,1.5,-0.8>
		}
	}
	rotate <0,360*clock,0>
}