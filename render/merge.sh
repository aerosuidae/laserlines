#!/bin/bash

set -e

pov=$1
crop=$2

frames="${pov}-frames"

convert -loop 0 -delay 1 ${frames}/frame*png ${pov}.gif
convert ${frames}/frame01.png -crop ${crop} -resize 32x32 ${pov}-icon.png
convert ${frames}/frame01.png -crop ${crop} -resize 128x128 ${pov}-tech.png
convert ${frames}/frame01.png -crop ${crop} -resize 144x144 ${pov}-thumb.png
montage ${frames}/frame*.png -background none -tile 7x7 -geometry +0+0 ${pov}-frames.png
