#!/bin/bash
# http://www.imagemagick.org/Usage/masking/#two_background

set -e

pov=$1

out="${pov}-tmp"
rm -rf ${out}
mkdir -p ${out}

for pair in 'north:0.5' 'east:0.75' 'south:0' 'west:0.25'; do
	dir=$(echo $pair | sed 's/:.*//')
	clock=$(echo $pair | sed 's/.*://')
	povray -D0 +A0.0 +AM2 +J -H256 -W256 +K${clock} DECLARE=Pass=1 +Fn +O${out}/${dir}1.png -I${pov}.pov
	povray -D0 +A0.0 +AM2 +J -H256 -W256 +K${clock} DECLARE=Pass=2 +Fn +O${out}/${dir}2.png -I${pov}.pov
	convert ${out}/${dir}1.png ${out}/${dir}2.png -alpha off \
		\( -clone 0,1 -compose difference -composite \
			-separate -evaluate-sequence max -auto-level -negate \) \
		\( -clone 0,2 -fx "v==0?0:u/v-u.p{0,0}/v+u.p{0,0}" \) \
			-delete 0,1 +swap -compose Copy_Opacity -composite \
		${pov}-${dir}.png
	convert ${pov}-${dir}.png -resize 96x96+0+0 ${pov}-${dir}-hr.png
	convert ${pov}-${dir}.png -resize 48x48+0+0 ${pov}-${dir}-lr.png
done

convert ${pov}-south.png -resize 32x32 ${pov}-icon.png
convert ${pov}-south.png -resize 128x128 ${pov}-tech.png
convert ${pov}-south.png -resize 144x144 ${pov}-thumb.png
