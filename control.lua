

local function serialize(t)
	local s = {}
	for k,v in pairs(t) do
		if type(v) == "table" then
			v = serialize(v)
		end
		s[#s+1] = tostring(k).." = "..tostring(v)
	end
	return "{ "..table.concat(s, ", ").." }"
end

local function queue()
	return {first = 0, last = -1}
end

local function qlen(list)
	return math.max(0, list.last - list.first + 1)
end

local function first(list)
	return list[list.first]
end

local function last(list)
	return list[list.last]
end

local function shove (list, value)
	local first = list.first - 1
	list.first = first
	list[first] = value
end

local function push (list, value)
	local last = list.last + 1
	list.last = last
	list[last] = value
end

local function shift (list)
	local first = list.first
	if first > list.last then return nil end
	local value = list[first]
	list[first] = nil
	list.first = first + 1
	return value
end

local function pop (list)
	local last = list.last
	if list.first > last then return nil end
	local value = list[last]
	list[last] = nil
	list.last = last - 1
	return value
end

local function distance(a, b)
	local x = b.x - a.x
	local y = b.y - a.y
	return math.sqrt(x*x + y*y)
end

local tqueue = nil

local function check_state()
	global.emitters = global.emitters or {}
	global.targets = global.targets or {}
end

local function validate_state()
	for id, state in pairs(global.emitters) do
		if not (state and state.emitter and state.emitter.valid) then
			global.emitters[id] = nil
		end
	end
	for id, emitters in pairs(global.targets) do
		(function()
			for emitter, _ in pairs(emitters) do
				if not (emitter and emitter.valid) then
					emitters[emitter] = nil
				end
			end
			for _, _ in pairs(emitters) do
				return
			end
			global.targets[id] = nil
		end)()
	end
end

local function emitter_state(emitter)
	local id = emitter.unit_number
	global.emitters[id] = global.emitters[id] or {
		emitter = emitter,
	}
	return global.emitters[id]
end

local function target_id(target)
	if target.valid then
		return target.unit_number or serialize({ target.name, target.position })
	end
end

local function is_target(target)
	return target.valid and global.targets[target_id(target)] ~= nil
end

local function target_state(target)
	local id = target_id(target)
	global.targets[id] = global.targets[id] or {}
	return global.targets[id]
end

local function target_purge(target)
	local id = target_id(target)
	for _, emitter in pairs(global.targets[id]) do
		return
	end
	global.targets[id] = nil
end

local function beam_area(position, direction, cbox, yonder)

	local tl = cbox.left_top or cbox[1]
	local br = cbox.right_bottom or cbox[2]
	local tx = tl.x or tl[1]
	local ty = tl.y or tl[2]
	local bx = br.x or br[1]
	local by = br.y or br[2]

	local area = {
		{ x = position.x + tx, y = position.y - yonder },
		{ x = position.x + bx, y = position.y + ty },
	}

	if direction == defines.direction.east then
		area = {
			{ x = position.x + bx, y = position.y + ty },
			{ x = position.x + yonder, y = position.y + by, },
		}
	end

	if direction == defines.direction.south then
		area = {
			{ x = position.x + tx, y = position.y + by },
			{ x = position.x + bx, y = position.y + yonder },
		}
	end

	if direction == defines.direction.west then
		area = {
			{ x = position.x - yonder, y = position.y + ty },
			{ x = position.x + tx, y = position.y + by, },
		}
	end

	area[1].x = area[1].x + 0.1
	area[1].y = area[1].y + 0.1
	area[2].x = area[2].x - 0.1
	area[2].y = area[2].y - 0.1

	return area
end

local colors = {
	["signal-red"] = "red",
	["signal-green"] = "green",
	["signal-blue"] = "blue",
	["signal-yellow"] = "yellow",
	["signal-pink"] = "pink",
	["signal-cyan"] = "cyan",
	["signal-white"] = "white",
}

local function emitter_beam(emitter, ignore)
	if not (emitter and emitter.valid) then
		return
	end

	local state = emitter_state(emitter)

	if state.beam then
		state.beam.destroy()
		state.beam = nil
	end

	if state.target and state.target.valid then
		local tstate = target_state(state.target)
		tstate[emitter] = nil
		target_purge(state.target)
		state.target = nil
	end

	if not emitter.get_control_behavior().enabled then
		return
	end

	local signal = emitter.get_control_behavior().get_signal(1)

	if not (signal and signal.signal) then

		local signals = emitter.get_merged_signals()
		if signals ~= nil then
			for _, v in ipairs(signals) do
				if v.signal.type == "virtual" and v.signal.name ~= nil and colors[v.signal.name] then
					signal = v
					break
				end
			end
		end

		if not (signal and signal.signal) then
			return
		end
	end

	local yonder = (signal.count > 1 and signal.count <= 1000 and signal.count) or settings.global["laserlines-range"].value or 300

	local targets = {}

	if settings.global["laserlines-obstructable"].value then
		targets = emitter.surface.find_entities_filtered({
			area = beam_area(emitter.position, emitter.direction, emitter.prototype.collision_box, yonder),
			collision_mask = { "player-layer" },
		})
	end

	if ignore then
		local reduced = {}
		for i, target in ipairs(targets) do
			if not ignore[target_id(target)] then
				reduced[#reduced+1] = target
			end
		end
		targets = reduced
	end

	local bump = {
		[defines.direction.north] = { 0, -0.7 },
		[defines.direction.east] = { 0.4, -0.2 },
		[defines.direction.south] = { 0, 0.1 },
		[defines.direction.west] = { -0.4, -0.2 },
	}

	local spos = { x = emitter.position.x + bump[emitter.direction][1], y = emitter.position.y + bump[emitter.direction][2] }

	local tpos = { x = spos.x, y = spos.y - yonder }

	if emitter.direction == defines.direction.east then
		tpos = { x = spos.x + yonder, y = spos.y }
	end

	if emitter.direction == defines.direction.south then
		tpos = { x = spos.x, y = spos.y + yonder }
	end

	if emitter.direction == defines.direction.west then
		tpos = { x = spos.x - yonder, y = spos.y }
	end

	if #targets > 0 then
		local target = emitter.surface.get_closest(emitter.position, targets)

		if distance(emitter.position, target.position) < yonder then
			local cbox = target.prototype.collision_box or { { -0.5, -0.5 }, { 0.5, 0.5 } }

			local tl = cbox.left_top or cbox[1]
			local br = cbox.right_bottom or cbox[2]
			local tx = tl.x or tl[1]
			local ty = tl.y or tl[2]
			local bx = br.x or br[1]
			local by = br.y or br[2]

			if emitter.direction == defines.direction.north then
				tpos = { x = spos.x, y = target.position.y + by }
			end

			if emitter.direction == defines.direction.east then
				tpos = { x = target.position.x + tx, y = spos.y }
			end

			if emitter.direction == defines.direction.south then
				tpos = { x = spos.x, y = target.position.y + ty }
			end

			if emitter.direction == defines.direction.west then
				tpos = { x = target.position.x + bx, y = spos.y }
			end

			local tstate = target_state(target)
			tstate[emitter] = emitter
			state.target = target
		end
	end

	local color = colors[signal.signal.name] or settings.global["laserlines-default-color"].value

	state.beam = emitter.surface.create_entity({
		name = "laserlines-beam-"..color,
		position = spos,
		force = emitter.force,
		source_position = spos,
		target_position = tpos,
	})

	return state.beam
end

local function emitters_affected(surface, position, cbox)
	local affected = {}
	for _, direction in ipairs({ defines.direction.north, defines.direction.east, defines.direction.south, defines.direction.west }) do
		local emitters = surface.find_entities_filtered({
			name = "laserlines-emitter",
			area = beam_area(position, direction, cbox, settings.global["laserlines-range"].value),
		})
		if #emitters > 0 then
			local emitter = surface.get_closest(position, emitters)
			if emitter and emitter.valid then
				affected[#affected+1] = emitter
			end
		end
	end
	return affected
end

local function emitters_update(emitters, ignore)
	for _, emitter in pairs(emitters) do
		if emitter.valid then
			emitter_beam(emitter, ignore)
		end
	end
end

local function OnEntityCreated(event)
	local entity = event.created_entity

	if not entity or not entity.valid then
		return
	end

	check_state()

	if entity.name == "laserlines-emitter" then

		local signal = entity.get_control_behavior().get_signal(1)

		if not (signal and signal.signal) and not settings.global["laserlines-listen"].value then
			entity.get_control_behavior().set_signal(1, {
				signal = { type = "virtual", name = "signal-"..settings.global["laserlines-default-color"].value },
				count = 1,
			})
		end

		emitters_update({entity})

		if settings.global["laserlines-listen"].value then
			push(tqueue, entity.unit_number)
		end
	end

	emitters_update(emitters_affected(entity.surface, entity.position, entity.prototype.collision_box))
end

local function OnEntityRemoved(event)
	local entity = event.entity

	if not entity or not entity.valid then
		return
	end

	check_state()

	if entity.name == "laserlines-emitter" then
		local beam = emitter_beam(entity)
		if beam then beam.destroy() end
		validate_state()
	end

	if is_target(entity) then
		emitters_update(target_state(entity), { [target_id(entity)] = true })
		validate_state()
	end
end

local function OnNthTick()

	if not tqueue then
		tqueue = queue()
		for id, _ in pairs(global.emitters) do
			push(tqueue, id)
		end
	end

	local id = shift(tqueue)
	local state = global.emitters[id]

	if state and state.emitter and state.emitter.valid then
		push(tqueue, id)
		emitters_update({state.emitter})
	end
end

local function set_tick_handler()
	script.on_nth_tick(nil)
	tqueue = nil
	if settings.global["laserlines-listen"].value then
		script.on_nth_tick(2, OnNthTick)
	end
end

local function OnChange(event)
	check_state()
	validate_state()
	set_tick_handler()
end

script.on_init(function()
	check_state()
	set_tick_handler()
	script.on_event({defines.events.on_built_entity, defines.events.on_robot_built_entity}, OnEntityCreated)
	script.on_event({defines.events.on_player_mined_entity, defines.events.on_robot_pre_mined, defines.events.on_entity_died}, OnEntityRemoved)
	script.on_event({defines.events.on_runtime_mod_setting_changed}, OnChange)
	script.on_configuration_changed(OnChange)
end)

script.on_load(function()
	set_tick_handler()
	script.on_event({defines.events.on_built_entity, defines.events.on_robot_built_entity}, OnEntityCreated)
	script.on_event({defines.events.on_player_mined_entity, defines.events.on_robot_pre_mined, defines.events.on_entity_died}, OnEntityRemoved)
	script.on_event({defines.events.on_runtime_mod_setting_changed}, OnChange)
	script.on_configuration_changed(OnChange)
end)

script.on_event({defines.events.on_gui_closed}, function(event)
	if event.entity and event.entity.valid and event.entity.name == "laserlines-emitter" then
		check_state()
		emitters_update({event.entity})
	end
end)

script.on_event({defines.events.on_entity_settings_pasted}, function(event)
	if event.destination and event.destination.valid and event.destination.name == "laserlines-emitter" then
		check_state()
		emitters_update({event.destination})
	end
end)

script.on_event({defines.events.on_player_rotated_entity}, function(event)
	if event.entity and event.entity.valid and event.entity.name == "laserlines-emitter" then
		check_state()
		emitters_update({event.entity})
	end
end)
